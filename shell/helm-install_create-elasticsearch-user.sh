#!/bin/bash bash -e


while getopts n:u:p: flag
do
  case "${flag}" in
    n) namespace=$OPTARG;;
    u) elasticsearch_username=$OPTARG;;
    p) elasticsearch_password=$OPTARG;;
    *)
      echo "Error in CLI" >&2
      exit 1
  esac
done

if [ -z $namespace ] || [ -z $elasticsearch_username ] || [ -z $elasticsearch_password ]; then
  echo "Missing the argument '-n, -u or -e'" >&2
  exit 1
fi

function Execute {
  Execute_job
}


function Execute_job {
  ## CHECK IF NAMESPACE EXIST
  if ! oc project $namespace > /dev/null 2>&1; then
    echo "This namespace don't exist or you dont have permission"    
  else
      # file=$(echo $namespace|awk -F'-dsp' '{print $1}') ## If needed to delete word from the namespace name
      
      file=$namespace
      
      echo "Executing in the namespace: $namespace"
      echo "Using Helm values: Overrides/Elastic/values-$file.yaml"

      helm uninstall elk -n $namespace

      sleep 5

      helm install elk . -f ../Overrides/Elastic/values-${file}.yaml -n $namespace

      sleep 10

      oc port-forward elk-elastic-es-0 9200:9200 -n $namespace &

      mypid=$(jobs -p)

      sleep 10

      ## CREATE A ROLE WITH ACCESS TO ALL INDEX
      curl -u $elasticsearch_username:"$elasticsearch_password" -H 'Content-Type: application/json' --request POST http://localhost:9200/_security/role/report_role --data '{
            "cluster": ["all"],
            "indices": [
              {
                "names": [ "*" ],
                "privileges": ["read", "write"]
                }
            ]
          }'


      sleep 5;

      ## CREATE A USER TO ACCESS AND MANAGE KIBANA
      curl -u $elasticsearch_username:"$elasticsearch_password" -H 'Content-Type: application/json' --request POST http://localhost:9200/_security/user/report --data '{
            "password" : "report",
            "enabled": true,
            "roles" : [ "kibana_user", "kibana_admin", "report_role", "reporting_user" ],
            "full_name" : "Kibana User",
            "email" : "",
            "metadata" : {
              "intelligence" : 7
            }
          }'

      kill -9 $mypid
  fi
}

Execute
